#!/bin/bash
#
# Thermald is a systemd unit file that requires specific hardware and
# firmware.  As such it is difficult to test in an automated fashion.

# fail the whole test if any of the command pipelines below fails
set -ex

rpmversion=$(rpm -q --queryformat '%{VERSION}' thermald)
version=$(thermald --version)
if [ "$version" != "$rpmversion" -a "$version.0" != "$rpmversion" -a "$version" != "$rpmversion.0" ]; then
	echo "reported thermald version (${version}) does not match rpm version (${rpmversion})"
	exit 1
fi

# if we get here, it's OK

